package com.example.RecruitmentManagement;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class RecruitmentManagementApplication {

	public static void main(String[] args) {

		SpringApplicationBuilder builder = new SpringApplicationBuilder(RecruitmentManagementApplication.class);
		builder.headless(false).run(args);
	}

}
