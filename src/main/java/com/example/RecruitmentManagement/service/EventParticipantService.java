package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.EventParticipant;

import java.util.List;

public interface EventParticipantService {
    void addEventParticipant(EventParticipant eventParticipant);
    void updateEventParticipant(EventParticipant eventParticipant, Long id);

    void deleteEventParticipant(Long id);
    EventParticipant getEventParticipantById(Long id);
    List<EventParticipant> getAllEventParticipants();
}
