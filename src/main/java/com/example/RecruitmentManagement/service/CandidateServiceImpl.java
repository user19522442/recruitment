package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidateServiceImpl implements CandidateService{

    @Autowired
    private CandidateRepository candidateRepository;
    @Override
    public void addCandidate(Candidate candidate) {
        if (candidate != null){
            candidateRepository.save(candidate);
        }
    }

    @Override
    public void updateCandidate(Candidate candidate, Long id) {
        if (candidate != null) {
            Candidate existingCandidate = candidateRepository.findById(id).get();
            existingCandidate.setFirstName(candidate.getFirstName());
            existingCandidate.setLastName(candidate.getLastName());
            existingCandidate.setPhone(candidate.getPhone());
            existingCandidate.setAddress(candidate.getAddress());
            existingCandidate.setSex(candidate.getSex());
            existingCandidate.setDateOfBirth(candidate.getDateOfBirth());
            candidateRepository.save(existingCandidate);
        }
    }

    @Override
    public void deleteCandidate(Long id) {
        Candidate existingCandidate = candidateRepository.findById(id).get();
        candidateRepository.delete(existingCandidate);
    }

    @Override
    public Candidate getCandidateById(Long id) {

        Candidate existingCandidate = candidateRepository.findById(id).get();
        return  existingCandidate;

    }

    @Override
    public List<Candidate> getAllCandidates() {
        return candidateRepository.findAll();
    }
}
