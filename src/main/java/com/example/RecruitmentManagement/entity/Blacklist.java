package com.example.RecruitmentManagement.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "blacklists")
public class Blacklist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String reason;


    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "blacklist_id", referencedColumnName = "id")
    private List<Candidate> candidateList;
}
