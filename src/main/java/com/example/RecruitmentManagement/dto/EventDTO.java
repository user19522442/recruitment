package com.example.RecruitmentManagement.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventDTO {
    private String name;
    private Date timeEvent;
    private  String address;
    private String field;
    private String description;
    private String guestInformation;
    private String contact;
    private int number;
    private String guest;

}