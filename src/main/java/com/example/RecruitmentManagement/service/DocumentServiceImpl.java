package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.Exception.FileStorageException;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.entity.Document;
import com.example.RecruitmentManagement.repository.CandidateRepository;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import com.example.RecruitmentManagement.repository.DocumentRepository;

@Service
public class DocumentServiceImpl implements DocumentService{
    @Autowired
    private DocumentRepository documentRepository;
    @Autowired
    private CandidateRepository candidateRepository;
    @Autowired
    private Storage storage;
    private String bucketName;

    @Autowired
    public void FileUploadService(Storage storage, @Value("${firebase.storage.bucket}") String bucketName){
        this.storage = storage;
        this.bucketName = bucketName;
    }
    @Override
    public Document saveDocument(MultipartFile file, Long id) {
        String originalFilename = file.getOriginalFilename();
        if (originalFilename != null) {
            String fileName = StringUtils.cleanPath(originalFilename);
            try {
                if (fileName.contains("..")) {
                    throw new FileStorageException("Filename contains invalid path sequence "
                            + fileName);
                }

                BlobInfo blobInfo = BlobInfo.newBuilder("quanlytuyendung-b9f07.appspot.com", fileName)
                        .setContentType(file.getContentType())
                        .build();
                storage.create(blobInfo, file.getBytes());
                String url = String.format("https://firebasestorage.googleapis.com/v0/b/%s/o/%s?alt=media", bucketName, fileName);
                Document document = new Document();
                Candidate candidate = candidateRepository.findById(id).orElse(null);
                if (candidate != null) {
                    document.setName(fileName);
                    document.setUrl(url);
                    document.setFileType(file.getContentType());
                    document.setCandidate(candidate);
                    return documentRepository.save(document);
                }

            } catch (Exception e) {
                throw new FileStorageException("Could not save File: " + fileName);
            }
        }
        return null;
    }

    @Override
    public Document getDocument(Long id) {
        Document document = documentRepository.findById(id).orElse(null);
        return document;
    }

    @Override
    public void updateDocument(Document document, Long id) {
        Document existingDocument = documentRepository.findById(id).orElse(null);
        if (existingDocument != null) {
            existingDocument.setName(document.getName());
            existingDocument.setFileType(document.getFileType());
            existingDocument.setUrl(document.getUrl());
            existingDocument.setCandidate(document.getCandidate());
            documentRepository.save(existingDocument);
        }
    }
}
