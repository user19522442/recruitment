package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Blacklist;
import com.example.RecruitmentManagement.repository.BlacklistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlacklistServiceImpl implements BlacklistService {
    @Autowired
    private BlacklistRepository blacklistRepository;

    @Override
    public List<Blacklist> getAllBlacklists() {
        return blacklistRepository.findAll();
    }

    @Override
    public Blacklist getBlacklistById(Long blacklistId) {
        return blacklistRepository.findById(blacklistId)
                .orElseThrow(() -> new RuntimeException("Blacklist not found with id: " + blacklistId));
    }

    @Override
    public void addBlacklist(Blacklist blacklist) {
        blacklistRepository.save(blacklist);
    }

    @Override
    public void deleteBlacklist(Long blacklistId) {
        blacklistRepository.deleteById(blacklistId);
    }
}
