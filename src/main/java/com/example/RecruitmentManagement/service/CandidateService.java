package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Candidate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CandidateService {
    void addCandidate(Candidate candidate);
    void updateCandidate(Candidate candidate, Long id);
    void deleteCandidate(Long id);
    Candidate getCandidateById(Long id);
    List<Candidate> getAllCandidates();
}
