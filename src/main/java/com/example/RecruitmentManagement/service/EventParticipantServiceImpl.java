package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.EventParticipant;
import com.example.RecruitmentManagement.repository.EventParticipantResponsitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventParticipantServiceImpl implements EventParticipantService{
    @Autowired
    private EventParticipantResponsitory eventParticipantResponsitory;

    @Override
    public void addEventParticipant(EventParticipant eventParticipant) {
        if (eventParticipant != null) {
            eventParticipantResponsitory.save(eventParticipant);
        }
    }

    @Override
    public void updateEventParticipant(EventParticipant eventParticipant, Long id) {
        if (eventParticipant != null) {
            EventParticipant existingEventParticipant = eventParticipantResponsitory.findById(id).get();
            //existingEventParticipant.setName(eventParticipant.getName());
            //existingEventParticipant.setEmail(eventParticipant.getEmail());
            //existingEventParticipant.setJobPosition(eventParticipant.getJobPosition());
            //existingEventParticipant.setLevel(eventParticipant.getLevel());
            existingEventParticipant.setFeedback(eventParticipant.getFeedback());
            eventParticipantResponsitory.save(existingEventParticipant);
        }
    }

    @Override
    public void deleteEventParticipant(Long id) {
        EventParticipant existingEventParticipant = eventParticipantResponsitory.findById(id).get();
        eventParticipantResponsitory.delete(existingEventParticipant);
    }

    @Override
    public EventParticipant getEventParticipantById(Long id) {
        EventParticipant existingEventParticipant = eventParticipantResponsitory.findById(id).get();
        return  existingEventParticipant;
    }

    @Override
    public List<EventParticipant> getAllEventParticipants() {
            return eventParticipantResponsitory.findAll();
    }
}
