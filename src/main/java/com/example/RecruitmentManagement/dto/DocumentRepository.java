package com.example.RecruitmentManagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DocumentRepository {
    private String fileName;
    private String downloadURL;
    private String fileType;
    private long fileSize;
}
