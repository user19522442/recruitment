package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.ApiResponse;
import com.example.RecruitmentManagement.dto.DocumentRepository;
import com.example.RecruitmentManagement.entity.Document;
import com.example.RecruitmentManagement.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/document")
public class DocumentController {
    @Autowired
    private DocumentService documentService;
    @PostMapping("/upload/{candidateId}") //Tải lên CV với Candidate Id của tài khoản đang đăng nhập
    @PreAuthorize("hasAuthority('ROLE_CANDIDATE')")
    public ResponseEntity<ApiResponse> uploadDocument(@RequestParam("file") MultipartFile file, @PathVariable("candidateId") Long candidateId) {
        Document document = documentService.saveDocument(file, candidateId); //Lưu document vào database và trả về object vừa lưu

        DocumentRepository cvRepository = new DocumentRepository(document.getName(),document.getUrl(), document.getFileType(), file.getSize());

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("CV was uploaded successfully")
                .data(cvRepository)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
