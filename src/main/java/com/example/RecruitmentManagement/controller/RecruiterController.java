package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.ApiResponse;
import com.example.RecruitmentManagement.entity.Recruiter;
import com.example.RecruitmentManagement.service.RecruiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/recruiter")
public class RecruiterController {
    @Autowired
    private RecruiterService recruiterService;

    private static final String RECRUITER_NOT_FOUND_MESSAGE = "Recruiter not found with id: ";

    @PostMapping("/create")
    public ResponseEntity<ApiResponse> createRecruiter(@RequestBody Recruiter recruiter) {
        recruiterService.createRecruiter(recruiter);
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Recruiter created successfully")
                .data(null)
                .build();
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ApiResponse> updateRecruiter(@RequestBody Recruiter recruiter, @PathVariable("id") Long id) {
        Recruiter existingRecruiter = recruiterService.getRecruiter(id);
        if (existingRecruiter == null) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(RECRUITER_NOT_FOUND_MESSAGE + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        recruiterService.updateRecruiter(recruiter, id);

        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Recruiter updated successfully")
                .data(null)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ApiResponse> deleteRecruiter(@PathVariable("id") Long id) {
        Recruiter existingRecruiter = recruiterService.getRecruiter(id);
        if (existingRecruiter == null) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(RECRUITER_NOT_FOUND_MESSAGE + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        recruiterService.deleteRecruiter(id);
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Recruiter deleted successfully")
                .data(null)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ApiResponse> getRecruiter(@PathVariable("id") Long id) {
        Recruiter recruiter = recruiterService.getRecruiter(id);
        if (recruiter == null) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message(RECRUITER_NOT_FOUND_MESSAGE + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Recruiter retrieved successfully")
                .data(recruiter)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("")
    public ResponseEntity<ApiResponse> getAllRecruiters() {
        List<Recruiter> recruiters = recruiterService.getListRecruiters();
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Recruiters retrieved successfully")
                .data(recruiters)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/count")
    public ResponseEntity<ApiResponse> getCountRecruiters() {
        Integer count = recruiterService.getCountRecruiters();
        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Count of recruiters retrieved successfully")
                .data(count)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/search")
    public ResponseEntity<ApiResponse> searchRecruiters(@RequestParam("keyword") String keyword) {
        List<Recruiter> searchResults = recruiterService.searchRecruitersByName(keyword);

        if (searchResults.isEmpty()) {
            ApiResponse response = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message("No recruiters found with the given search term: " + keyword)
                    .data(null)
                    .build();
            return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        ApiResponse response = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Recruiters found successfully")
                .data(searchResults)
                .build();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
