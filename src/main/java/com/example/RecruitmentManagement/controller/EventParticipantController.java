package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.ApiResponse;
import com.example.RecruitmentManagement.entity.EventParticipant;
import com.example.RecruitmentManagement.service.EventParticipantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/eventParticipant")
public class EventParticipantController {
    @Autowired
    private EventParticipantService eventParticipantService;

    @PostMapping("/create")
    public ResponseEntity<?> addEventParticipant(@RequestBody EventParticipant eventParticipant) {
        eventParticipantService.addEventParticipant(eventParticipant);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("participate successful")
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCandidate(@RequestBody EventParticipant eventParticipant, @PathVariable("id") Long id){
        eventParticipantService.updateEventParticipant(eventParticipant, id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Participant updated successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCandidate(@PathVariable("id") Long id){
        eventParticipantService.deleteEventParticipant(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Candidate deleted successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<?> getParticipantById(@PathVariable("id")  Long id) {
        EventParticipant existingEventParticipant = eventParticipantService.getEventParticipantById(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Get candidate by id successful")
                .data(existingEventParticipant)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("")
    public  ResponseEntity<?> getAllEventParticipant(){
        List<EventParticipant> eventParticipantList = eventParticipantService.getAllEventParticipants();

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Get all candidate successful")
                .data(eventParticipantList)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

}
