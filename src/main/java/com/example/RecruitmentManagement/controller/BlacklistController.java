package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.ApiResponse;
import com.example.RecruitmentManagement.entity.Blacklist;
import com.example.RecruitmentManagement.service.BlacklistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/blacklist")
public class BlacklistController {
    @Autowired
    private BlacklistService blacklistService;

    @GetMapping
    public ResponseEntity<?> getAllBlacklists() {
        List<Blacklist> blacklists = blacklistService.getAllBlacklists();
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("The blacklist has been successfully obtained")
                .data(blacklists)
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getBlacklistById(@PathVariable Long id) {
        Blacklist blacklist = blacklistService.getBlacklistById(id);
        if (blacklist != null) {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.OK.value())
                    .message("Blacklist with id " + id + " has been obtained successfully")
                    .data(blacklist)
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.OK);
        } else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.NOT_FOUND.value())
                    .message("Can't find blacklist with id " + id)
                    .data(null)
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/create")
    public ResponseEntity<?> addBlacklist(@RequestBody Blacklist blacklist) {
        if (blacklist != null) {
            blacklistService.addBlacklist(blacklist);
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.CREATED.value())
                    .message("Blacklist has been created successfully")
                    .data(null)
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.CREATED);
        } else {
            ApiResponse apiResponse = ApiResponse.builder()
                    .statusCode(HttpStatus.BAD_REQUEST.value())
                    .message("Cannot create blacklist with invalid data")
                    .data(null)
                    .build();
            return new ResponseEntity<>(apiResponse, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteBlacklist(@PathVariable Long id) {
        blacklistService.deleteBlacklist(id);
        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.OK.value())
                .message("Blacklist with id " + id + " has been successfully deleted")
                .data(null)
                .build();
        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
