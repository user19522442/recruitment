package com.example.RecruitmentManagement.repository;
import com.example.RecruitmentManagement.entity.EventParticipant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EventParticipantResponsitory extends JpaRepository <EventParticipant, Long>{
}
