package com.example.RecruitmentManagement.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "candidates")
public class Candidate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String phone;
    private String address;
    private String sex;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date dateOfBirth;
    private boolean is_banned;

    @JsonIgnore
    @OneToOne(mappedBy = "candidate", cascade = CascadeType.ALL)
    private InterviewProcess interviewProcess;

    @OneToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "candidate_id",referencedColumnName = "id")
    private List<HistoryApply> historyApplies;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "candidate_id",referencedColumnName = "id")
    private List<Document> CVList;

    @JsonIgnore
    @ManyToMany(mappedBy = "candidates")
    private Set<Event> events = new HashSet<>();

    @JsonIgnore
    @OneToMany(mappedBy = "candidate", cascade = CascadeType.ALL)
    private List<Document> documentList;

    @JsonIgnore
    @OneToOne(mappedBy = "candidate", cascade = CascadeType.ALL)
    private EventParticipant eventParticipant;
}
