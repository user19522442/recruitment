package com.example.RecruitmentManagement.controller;

import com.example.RecruitmentManagement.dto.ApiResponse;
import com.example.RecruitmentManagement.entity.Candidate;
import com.example.RecruitmentManagement.service.CandidateServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/candidate")
public class CandidateController {
    @Autowired
    private CandidateServiceImpl candidateService;
    @PostMapping("/create")
    public ResponseEntity<?> addCandidate(@RequestBody Candidate candidate){
        candidateService.addCandidate(candidate);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Candidate created successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateCandidate(@RequestBody Candidate candidate, @PathVariable("id") Long id){
        candidateService.updateCandidate(candidate, id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Candidate updated successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteCandidate(@PathVariable("id") Long id){
        candidateService.deleteCandidate(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Candidate deleted successful")
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public  ResponseEntity<?> getCandidateById(@PathVariable("id")  Long id) {
        Candidate existingCandidate = candidateService.getCandidateById(id);

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Get candidate by id successful")
                .data(existingCandidate)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }

    @GetMapping()
    public  ResponseEntity<?> getAllCandidate(){
        List<Candidate> candidateList = candidateService.getAllCandidates();

        ApiResponse apiResponse = ApiResponse.builder()
                .statusCode(HttpStatus.CREATED.value())
                .message("Get all candidate successful")
                .data(candidateList)
                .build();

        return new ResponseEntity<>(apiResponse, HttpStatus.OK);
    }
}
