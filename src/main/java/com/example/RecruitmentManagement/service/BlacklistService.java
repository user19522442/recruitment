package com.example.RecruitmentManagement.service;

import com.example.RecruitmentManagement.entity.Blacklist;

import java.util.List;

public interface BlacklistService {
    List<Blacklist> getAllBlacklists();

    Blacklist getBlacklistById(Long blacklistId);

    void addBlacklist(Blacklist blacklist);

    void deleteBlacklist(Long blacklistId);
}